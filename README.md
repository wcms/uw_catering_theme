# Theme structure

This theme is a subtheme of uw_base_theme.

Source CSS files are located in libraries/ and are transformed using PostCSS.
Processed files are placed in the css/ directory.

##PostCSS plugins

PostCSS plugins used are:

*postcss-nesting* - Enables future-native CSS style nesting. Unlike SASS this
  requires `&` before all selectors.
*postcss-selector-matches* - enables future-native `:matches()` to generate
  pseudo-classes e.g. `:matches(:hover,:focus)`
*postcss-mixins* and *postcss-import* - enable use of mixins. See
  global/mixins.css file for available mixins. The mixins file must be imported
  into a source CSS file using postcss-import in order for these to be available.

*Note:* 'future-native' refers to CSS features that are currently part of
proposed CSS specifications but are not yet supported by browsers. When these
are supported the PostCSS plugins may no longer be needed.

#JavaScript plugins

The tab interface on the menu group landing pages is handled using the A11y
tabs JavaScript plugin here:

https://a11y.nicolas-hoffmann.net/tabs/

The mobile menu is handled using the Responsive Menu Combined module included
with the uw base platform, with some custom CSS modifiations. 
