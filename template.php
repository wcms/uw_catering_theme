<?php

/**
 * @file
 * Theme functions for uWaterloo Catering Theme.
 */

/**
 * Implements template_preprocess_page.
 */
function uw_catering_theme_preprocess_html(&$variables) {

  $meta_mobile_view = [
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => [
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1',
    ],
  ];
  drupal_add_html_head($meta_mobile_view, 'meta_mobile_view');
}

/**
 * Implements hook_css_alter().
 */
function uw_catering_theme_css_alter(&$css) {
  /* Unset derfault eu_cookie_compliance CSS */
  unset($css[drupal_get_path('module', 'eu_cookie_compliance') . '/css/eu_cookie_compliance.css']);
}

/**
 * Implements template_preprocess_page().
 */
function uw_catering_theme_preprocess_page(&$variables) {
  // This was forced in somewhere in the platform.
  unset($variables['page']['content']['uw_social_media_sharing_social_media_block']);
}

/**
 * Implements template_preprocess_node.
 */
function uw_catering_theme_preprocess_node(&$variables) {
  $viewmode = $variables['view_mode'];
  // Create a template suggestion for all view modes.
  $variables['theme_hook_suggestions'][] = 'node__' . $viewmode;
  $variables['theme_hook_suggestions'][] = 'node__' . $variables['node']->type . '__' . $viewmode;

}

/**
 * Implements template_preprocess_field.
 */
function uw_catering_theme_preprocess_field(&$variables) {
  // Add the Pricing Type suffix to the price if needed.
  $fieldname = $variables['element']['#field_name'];
  $parent_entity = $variables['element']['#object'];

  // Adjust heading levels depending on whether this is a stand-alone menu
  // Use path args to get node id (no better way to get the node object)
  if ((arg(0) == 'node') &&
      (($fieldname == 'field_menu_section_title') || ($fieldname == 'field_menu_item_title')
    )) {
      $node = node_load(arg(1));
      $standalone_field = field_get_items('node', $node, 'field_stand_alone_menu');
      $variables['parent_menu_stand_alone'] = $standalone_field[0]['value'];
  }

  // Menu price can be on the menus or on the paragraph item.
  if ($fieldname == 'field_menu_price') {
    if (isset($parent_entity->type) && ($parent_entity->type == 'menu')) {
      $entity_type = 'node';
    }
    else {
      $entity_type = 'paragraphs_item';
    }
    $price_type_field = field_get_items($entity_type, $parent_entity, 'field_price_type');

    if ($price_type_field[0]['value'] == 'pp') {
      $variables['items'][0]['#markup'] = $variables['items'][0]['#markup'] . '<span class="price-label">' . t('per person') . '</span>';
    }
  }

}

/**
 * Implements theme_menu_link.
 */
function uw_catering_theme_menu_link(&$variables) {
  $element = $variables['element'];
  $sub_menu = '';
  $toggle = '';
  $menu_name = $variables['element']['#original_link']['menu_name'];
  if ($menu_name == 'main-menu') {
    // Adds toggle span if there are child items.
    if ($element['#below']) {
      $sub_menu = drupal_render($element['#below']);
      $toggle = '<span class="toggle" tabindex="0" aria-expanded="false"><span class="visually-hidden">' . t('Toggle menu') . '</span></span>';
    }
  } else {
    $toggle = "";
  }
    $output = l($element['#title'], $element['#href'], $element['#localized_options']);
    return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $toggle . $sub_menu . "</li>\n";
}
