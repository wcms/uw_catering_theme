(function ($) {
  Drupal.behaviors.cateringHeader = {
    attach: function (context, settings) {

      // Shrink header on scroll
      var $header = $('header.header');
      // use a boolean var to check if the element is already shrinked
      var headerShrink = false;

      $(window).on('scroll', function () {
        if ($(this).scrollTop() > 1){
          if(!headerShrink) {
            $header.addClass('shrink');
            headerShrink = true;
          }
        }
        else {
          if (headerShrink) {
            $header.removeClass('shrink');
            headerShrink = false;
          }
        }
      });

      /**
       * Dropdown menu keyboard functionality
       */
      function toggleMenu(e, $this) {
        e.preventDefault();
        if ($this.hasClass('open')) {
          $this.removeClass('open');
          $this.attr('aria-expanded', 'false');
        } else {
          $('.site-menu .expanded .toggle.open').removeClass('open');
          $this.addClass('open');
          $this.attr('aria-expanded', 'true');
        }
      }

      $('.site-menu .expanded .toggle').on('touchstart mousedown', function (e) {
        toggleMenu(e, $(this));
      });

      // Keyboard behaviour
      $('.site-menu .expanded .toggle', context).on('keyup', function (e) {
        if (e.which == 13) {
          toggleMenu(e, $(this));
        }
      });

    }
  };

})(jQuery);
