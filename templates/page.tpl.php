<?php

/**
 * @file
 * Template file for pages.
 */
?>


<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <header class="header">
  <div id="skip" class="skip-links">
    <a href="#main" class="element-invisible element-focusable"><?php print t('Skip to main'); ?></a>
    <a href="#footer" class="element-invisible element-focusable"><?php print t('Skip to footer'); ?></a>
  </div>

    <?php if (!empty($page['header'])) : ?>
      <div class="fixwidth">
        <?php if ($is_front): ?>
          <h1>
        <?php endif; ?>
        <a href="<?php print $base_path; ?>" class="logo">
          <img src="<?php print $base_path; ?>/<?php print $directory; ?>/logo.svg" alt="<?php print $site_name;?>" />
        </a>
        <?php if ($is_front): ?>
          </h1>
        <?php endif; ?>
        <?php print render($page['header']); ?>
      </div>
    <?php endif; ?>
  </header>

  <main class="main" id="main">
    <div class="banner">
      <?php print render($page['banner']); ?>
      <?php if (!isset($node)) : ?>
        <picture>
          <source srcset="
            <?php print $base_path . $directory; ?>/assets/default-banner--sm.jpg 400w,
            <?php print $base_path . $directory; ?>/assets/default-banner--md.jpg 1000w,
            <?php print $base_path . $directory; ?>/assets/default-banner--wide.jpg 1500w" sizes="100vw">
          <img src="<?php print $base_path . $directory; ?>/assets/default-banner--wide.jpg" alt=" ">
        </picture>
      <?php endif; ?>
      <div class="fixwidth page-title-wrapper">
        <?php if (!$is_front) : ?>
          <h1 class="page-title"><?php print $title ? $title : $site_name; ?></h1>
        <?php endif; ?>
      </div>
    </div>

    <!-- in Drupal 8 this will become a region: -->
    <div class="content-wrapper">
      <?php print $messages; ?>
      <?php print render($page['help']); ?>
      <?php if ($tabs) : ?>
      <div class="tabs"><?php print render($tabs); ?></div>
      <?php endif; ?>
      <?php if ($action_links) : ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>

      <?php print render($page['content']) ?>
    </div>
  </main>


  <footer class="footer" id="footer">
    <?php if ($page['footer_upper']) : ?>
      <div class="footer-upper-wrapper">
        <?php print render($page['footer_upper']); ?>
      </div>
    <?php endif; ?>

    <div class="footer-contact">
      <div class="fixwidth">
        <?php print render($page['footer']); ?>
        <div class="uwaterloo-logo">
          <a href="http://uwaterloo.ca"><img src="<?php print $base_path;?>/<?php print $directory; ?>/assets/uwaterloo-logo.png" alt="<?php print t('University of Waterloo'); ?> " /></a>
        </div>
      </div>
    </div>
    <?php print render($page['footer_lower']); ?>
  </footer>

</div><!--/site-->
