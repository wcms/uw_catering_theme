<?php

/**
 * @file
 * Adds svg icon based on value of the field.
 */
?>
<ul class="<?php print $classes; ?> display-list inline-list dietary-icons"<?php print $attributes; ?>>
  <?php foreach ($items as $delta => $item) : ?>
    <?php $icon_name = str_replace(' ', '-', strtolower($item['#markup'])); ?>
    <li class="dietary-icon dietary-icon--<?php print $icon_name; ?>">
      <?php #For d8, do this using a twig embed to avoid duplicate ids #?>
      <svg class="icon" aria-hidden="true">
        <use xlink:href="#<?php print $icon_name; ?>" />
      </svg>
      <span class='dietary-label'><?php print $item['#markup']; ?></span>
    </li>
  <?php endforeach; ?>
</ul>
