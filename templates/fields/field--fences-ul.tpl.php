<?php

/**
 * @file
 * Wrap each field value in a <li>  and all of them in a <ul> element.
 */
?>
<?php if (!empty($element['#label_display'])) : ?>
  <div class="<?php print $classes; ?>"<?php print $attributes; ?>>
<?php endif; ?>

<?php if ($element['#label_display'] == 'inline') : ?>
  <span class="field-label"<?php print $title_attributes; ?>>
    <?php print $label; ?>:
  </span>
<?php elseif ($element['#label_display'] == 'above') : ?>
  <h2 class="field-label"<?php print $title_attributes; ?>>
    <?php print $label; ?>
  </h2>
<?php endif; ?>

<?php if (!empty($element['#label_display'])) : ?>
  <ul class="display-list">
<?php else : ?>
  <ul class="<?php print $classes; ?> display-list"<?php print $attributes; ?>>
<?php endif; ?>

  <?php foreach ($items as $delta => $item) : ?>
    <li<?php print $item_attributes[$delta]; ?>>
      <?php print render($item); ?>
    </li>
  <?php endforeach; ?>
</ul>

<?php if (!empty($element['#label_display'])) : ?>
  </div>
<?php endif;
