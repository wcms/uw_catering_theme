<?php

/**
 * @file
 * Field override for menu section title.
 */
?>

<?php foreach ($items as $delta => $item) : ?>
  <?php # set heading levels based on whether this is a stand-alone menu
    # or part of a group # ?>
  <?php if ($parent_menu_stand_alone == 0): ?>
    <?php $heading_level = 'h4' ?>
  <?php else: ?>
    <?php $heading_level = 'h3' ?>
  <?php endif; ?>

  <<?php print $heading_level; ?> class="menu-item-label <?php print $classes; ?>"<?php print $attributes; ?>>
    <?php print render($item); ?>
  </<?php print $heading_level; ?>>
<?php endforeach;
