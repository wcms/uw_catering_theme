<?php

/**
 * @file
 * Field override for menu links.
 */
?>

<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <ul class="display-list">
    <?php foreach ($items as $delta => $item) : ?>
      <li<?php print $item_attributes[$delta]; ?>>
        <?php print render($item); ?>
      </li>
    <?php endforeach; ?>
  </ul>
</div>
