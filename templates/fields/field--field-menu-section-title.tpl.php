<?php

/**
 * @file
 * Field override for menu section title.
 */
?>

<?php foreach ($items as $delta => $item) : ?>
  <?php # set heading levels based on whether this is a stand-alone menu
    # or part of a group # ?>
  <?php if ($parent_menu_stand_alone == 0): ?>
    <?php $heading_level = 'h3' ?>
  <?php else: ?>
    <?php $heading_level = 'h2' ?>
  <?php endif; ?>

  <<?php print $heading_level; ?> class="menu-section-heading <?php print $classes; ?>"<?php print $attributes; ?>>
    <span class="text-wrapper"><?php print render($item); ?></span>
  </<?php print $heading_level; ?>>
<?php endforeach;
