<?php

/**
 * @file
 * Theme implementation to display a Menu Group node.
 *
 * Renders the in-page nav at the top.
 *
 * @ingroup themeable
 */
?>
<?php $classes .= ' ' . $view_mode; ?>
<?php
  $menu_ref_fields = field_get_items('node', $node, 'field_menu_ref');
?>
<div class="js-tabs">
  <ul class="menu-nav display-list js-tablist" data-existing-hx="h2">
    <?php foreach ($menu_ref_fields as $key => $menu_ref) : ?>

      <!-- id should output like 'label_menu-0' -->
      <li class="js-tablist__item">
        <a class="js-tablist__link" id="label_menu-<?php print $key; ?>" href="#menu-<?php print $key; ?>">
          <?php print($menu_ref['entity']->title);?>
        </a>
      </li>
    <?php endforeach; ?>
  </ul>

  <div class="<?php print $classes; ?>"<?php print $attributes; ?>>
    <?php
    hide($content['links']);
    hide($content['comments']);
    hide($content['field_menu_ref']);
    print render($content);
    ?>
    <!-- id should output like 'label_menu-0' -->
    <?php foreach ($menu_ref_fields as $key => $menu_ref) : ?>
      <div class="js-tabcontent" id="menu-<?php print $key; ?>">
        <?php print render($content['field_menu_ref'][$key]);?>
      </div>
    <?php endforeach; ?>

  </div>
</div>
