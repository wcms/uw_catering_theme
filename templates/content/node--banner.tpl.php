<?php

/**
 * @file
 * Custom theme implementation to display a node using Banner view mode.
 *
 * Uses <div> wrapper, does not display title.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<?php $classes .= ' ' . $view_mode; ?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <div class="content"<?php print $content_attributes; ?>>
    <?php print render($content); ?>
    <?php if (!(isset($content['field_banner_image']['#items'][0])) && ($node->type != 'uw_catering_home_page')) : ?>
      <div class="default-banner">
        <?php
        // Default banner image. ?>
        <?php global $base_path; ?>
        <?php
        if (($node->type == 'menu' || $node->type == 'menu_group')) {
          $img_prefix = '--menu';
        }
        else {
          $img_prefix = '';
        }
        ?>

        <picture>
            <source srcset="
              <?php print $base_path; ?>/<?php print $directory; ?>/assets/default-banner<?php print $img_prefix; ?>--sm.jpg 400w,
              <?php print $base_path; ?>/<?php print $directory; ?>/assets/default-banner<?php print $img_prefix; ?>--md.jpg 1000w,
              <?php print $base_path; ?>/<?php print $directory; ?>/assets/default-banner<?php print $img_prefix; ?>--wide.jpg 1500w"
              media="all" type="image/jpeg" sizes="100vw">
            <img src="<?php print $directory; ?>/assets/default-banner<?php print $img_prefix; ?>--wide.jpg" alt="">
          </picture>
        </div>
    <?php endif; ?>
  </div>

</div>
